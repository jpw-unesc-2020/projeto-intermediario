const mongoose = require('../data');

const tokenSchema = new mongoose.Schema({
  usuario: {
    type: String,
    required: true,
  },
  token: {
    type: String,
    required: true,
    unique: true, 
  }
});

const Token = new mongoose.model('Token', tokenSchema, 'Token');

module.exports = Token;