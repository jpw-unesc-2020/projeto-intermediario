const mongoose = require('../data');

const competitionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  organization: {
    type: String,
    required: true,
  },
  edition: {
    type: Number,
    required: true,
  },
  system: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  date_initial: {
    type: Date,
    required: true,
  },
  date_end: {
    type: Date,
    required: true,
  },
  number_teams: {
    type: Number,
    required: true,
  },
  premium_value: {
    type: Number,
    required: true,  
  }
});

const Competition = new mongoose.model('Competition', competitionSchema, 'Competition');

module.exports = Competition;