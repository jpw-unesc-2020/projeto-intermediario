const mongoose = require('../data');

const teamSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  nickname: {
    type: String,
    required: true,
  },
  stadium: {
    type: String,
    required: true,  
  },
  mascot: {
    type: String,    
  },
  date_foundation: {
    type: Date,
    required: true,  
  },
  sponsor_master: {
    type: String,
  },
  location: {
    type: String,
    required: true,
  },
  number_fans: {
    type: Number,
    required: true
  }
});

const Team = new mongoose.model('Team', teamSchema, 'Team');

module.exports = Team;