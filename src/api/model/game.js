const mongoose = require('../data');

const gameSchema = new mongoose.Schema({
  team_one: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: true,
  },
  team_two: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: true,
  },
  competition: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Competition',
    required: true,
  },
  date_game: {
    type: Date,
    required: true,  
  },
  stadium: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  description: {
    type: String,  
  },
  goals_team_one: {
    type: Number,
  },
  goals_team_two: {
    type: Number,
  },  
});

const Game = new mongoose.model('Game', gameSchema, 'Game');

module.exports = Game;