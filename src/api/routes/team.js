const express = require('express');
const router = express.Router();
const TeamModel = require('../model/team');
const GameModel = require('../model/game');

router.get('/', function(req, res) {
  const filter = {
    $or: [
      req.query.name ? {name: new RegExp(`.*^${req.query.name}.*`, 'i')} : {}, 
      req.query.stadium ? {stadium: new RegExp(`.*^${req.query.stadium}.*`, 'i')} : {}, 
      req.query.sponsor_master ? {sponsor_master: new RegExp(`.*^${req.query.sponsor_master}.*`, 'i')} : {}, 
      req.query.location ? {location: new RegExp(`.*^${req.query.location }.*`, 'i')} : {}
    ],
  };
  
  TeamModel.find(filter, function(err, doc) {
    if (!err) {
      res.status(200).json(doc);
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });  
    }
  }).limit(parseInt(req.query.limit || 100));
});

router.get('/:id', function(req, res) {
  const id = req.params.id;
  
  TeamModel.findById(id, function(err, doc) {
    if (!err) {
      res.status(200).json(doc);
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });
    }
  });
});

router.post('/', function(req, res) {
  const team = req.body;
  const newTeam = new TeamModel(team);
  newTeam.save(function(err) {
    if (!err) {
      res.status(200).json(newTeam);
    } else {
      res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
    }
  });
});

router.put('/:id', function(req, res) {
  const id = req.params.id;
  const team = req.body;

  TeamModel.findByIdAndUpdate(id, team, function(err) {
    if (!err) {
      res.status(200).json(team);
    } else {
      res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
    }
  });
});

router.delete('/:id', function(req, res) {
  const id = req.params.id;

  GameModel.find({$or: [{ team_one: id }, { team_two: id }]}, function(err, doc) {
    if (!err) {
      if (doc.length === 0) {
        TeamModel.findByIdAndDelete(id, function(err, doc) {
          if (!err) {
            res.status(200).json(doc);
          } else {
            res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
          }
        });
      } else {
        res.status(204).json({ erro: 'Não é possível excluir um time vinculado a um jogo!' });
      }
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });
    }
  });
});

module.exports = router;