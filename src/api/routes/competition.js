const express = require('express');
const router = express.Router();
const CompetitionModel = require('../model/competition');
const GameModel = require('../model/game');

router.get('/', function(req, res) {
  const filter = {
    $or: [
      req.query.name ? { name: new RegExp(`.*${req.query.name}.*`, 'i') } : {}, 
      req.query.organization ? { organization: new RegExp(`.*${req.query.organization }.*`, 'i')} : {}, 
      req.query.location ? { location: new RegExp(`.*${req.query.location }.*`, 'i')} : {}
    ],
  };

  CompetitionModel.find(filter, function(err, doc) {
    if (!err) {
      res.status(200).json(doc);
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });  
    }
  }).limit(parseInt(req.query.limit || 100));
});

router.get('/:id', function(req, res) {
  const id = req.params.id;
  
  CompetitionModel.findById(id, function(err, doc) {
    if (!err) {
      res.status(200).json(doc);
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });
    }
  });
});

router.post('/', function(req, res) {
  const competition = req.body;
  const newCompetition = new CompetitionModel(competition);
  newCompetition.save(function(err) {
    if (!err) {
      res.status(200).json(newCompetition);
    } else {
      res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
    }
  });
});

router.put('/:id', function(req, res) {
  const id = req.params.id;
  const competition = req.body;

  CompetitionModel.findByIdAndUpdate(id, competition, function(err) {
    if (!err) {
      res.status(200).json(competition);
    } else {
      res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
    }
  });
});

router.delete('/:id', function(req, res) {
  const id = req.params.id;

  GameModel.find({competition: id}, function(err, doc) {
    if (!err) {
      if (doc.length === 0) {
        CompetitionModel.findByIdAndDelete(id, function(err, doc) {
          if (!err) {
            res.status(200).json(doc);
          } else {
            res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
          }
        });
      } else {
        res.status(204).json({ erro: 'Não é possível excluir uma competição vinculada a um jogo!' });
      }
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });
    }
  });
});

module.exports = router;