const express = require('express');
const router = express.Router();
const GameModel = require('../model/game');

router.get('/', function(req, res) {
  const filter = {
    //date_game: req.query.date_game,
    status: req.query.status,
    //id_competition: req.query.id_competition,
  };

  //Remover campos não usados
  Object.keys(filter).forEach((key) => {
    if (filter[key] === undefined) {
      delete filter[key];
    }
  });

  GameModel.find(filter, function(err, doc) {
    if (!err) {
      res.status(200).json(doc);
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });  
    }
  }).populate('team_one').populate('team_two').populate('competition').limit(parseInt(req.query.limit || 100));
});

router.get('/:id', function(req, res) {
  const id = req.params.id;
  
  GameModel.findById(id, function(err, doc) {
    if (!err) {
      res.status(200).json(doc);
    } else {
      res.status(500).json({ erro: 'Erro ao acessar recurso! Contate o responsável pela aplicação!' });
    }
  }).populate('team_one').populate('team_two').populate('competition');
});

router.post('/', function(req, res) {
  const game = req.body;
  const newGame = new GameModel(game);
  newGame.save(function(err) {
    if (!err) {
      res.status(200).json(newGame);
    } else {
      res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
    }
  });
});

router.put('/:id', function(req, res) {
  const id = req.params.id;
  const game = req.body;

  GameModel.findByIdAndUpdate(id, game, function(err) {
    if (!err) {
      res.status(200).json(game);
    } else {
      res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
    }
  });
});

router.delete('/:id', function(req, res) {
  const id = req.params.id;

  GameModel.findByIdAndDelete(id, function(err, doc) {
    if (!err) {
      res.status(200).json(doc);
    } else {
      res.status(500).json({ erro: 'Erro ao salvar! Contate o responsável pela aplicação!' });
    }
  });
});

module.exports = router;