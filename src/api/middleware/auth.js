const express = require('express');
const route = express.Router();
const Token = require('../model/token');

route.use(function(req, res, next) {
  const authorization = req.headers.authorization;

  if (!authorization) return res.status(400).json({ erro: 'Faltando authorization!' });

  const token = authorization.replace('Bearer ', '');

  if (!token) return res.status(400).json({ erro: 'Faltando o token!' });
  
  Token.findOne({ token: token }, function(err, doc) {
    if (err) return res.status(500).json({ erro: 'Erro ao pesquisar token!' });
    
    if (!doc) return res.status(401).json({ erro: 'Token não autorizado!' });

    next();
  });
});

module.exports = route;