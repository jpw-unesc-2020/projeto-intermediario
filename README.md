## Documentação API - PROJETO INTERMEDIÁRIO
Esta documentação tem por finalidade auxiliar a integração entre APIs e facilitar a usabilidade da PROJETO INTERMEDIÁRIO. Esta aplicação tem por finalidade registrar partidas de futebol dentro de uma competição. Portanto, sua arquitetura é composta por **partidas**, **times** e **competições**.

### Endpoint
O endpoint de conexão com a API é: http://localhost:3005

### Recursos disponíveis
Atualmente existem os seguintes recursos abaixo que você pode manipular através dos métodos GET, POST, PUT e DELETE:
* Times (team)
* Competição (competition)
* Partida (game)

### Tratamento de dados
Todos os dados enviados e recebidos pela API estão/deverão ser em formato JSON (application/json).

### Autenticação
Para poder trabalhar com os métodos disponíveis e assim fazer a integração você precisa ter acesso às chaves de integração REST que estão disponíveis na coleção do banco de dados MongoDB.

### Códigos de retorno
Na tabela abaixo estão listados os códigos de retorno das APIS.

| Código | Status                     |
| ------ | -------------------------- |
| 500    | Erro causado pelo servidor |
| 400    | Erro causado pelo cliente  |
| 200    | Sucesso                    |

## APIS DISPONÍVEIS

### TIME
 Este módulo é resposánvel por controlar os times da aplicação

 ##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: db5fb842672bb7fc1a105cf3ff645e861be2e05853a73cc022544c6c20649e57|
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR TIMES - @GET
 ```url
 http://localhost:3005/team
 ```
Listar todos os times: `http://localhost:3005/team`

Listar time único: `http://localhost:3005/team/:id`

###### QueryString das filtragens disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| stadium        | Estádio do time                                                                                   | 
| sponsor_master | Patrocinador master                                                                               | 
| location       | Local em que o time atual                                                                        | 

Exemplo de pesquisa com querystring: ```http://localhost:3005/team?stadium=Maracana&sponsor_master=Adidas&location=Criciuma```

##### SALVAR TIME - @POST
  ```url
 http://localhost:3005/team
 ```
 
| Atributo        | Tipo   | Obrigatório | 
| --------------- |--------|:-----------:| 
| name            | String | Sim         |
| nickname        | String | Sim         |
| stadium         | String | Sim         |
| mascot          | String | Não         |
| date_foundation | Date   | Sim         |
| sponsor_master  | String | Não         |
| location        | String | Sim         |
| number_fans     | Number | Sim         |

##### ALTERAR TIME - @PUT
```url
http://localhost:3005/team/:id
```
Para alterar um time você precisa informar na URL do endpoint um {id} válido.

| Atributo        | Tipo   | Obrigatório |
| --------------- |--------|:-----------:|
| name            | String | Sim         |
| nickname        | String | Sim         |
| stadium         | String | Sim         |
| mascot          | String | Não         |
| date_foundation | Date   | Sim         |
| sponsor_master  | String | Não         |
| location        | String | Sim         |
| number_fans     | Number | Sim         |

##### EXCLUIR TIME - @DELETE
```url
http://localhost:3005/team/:id
```
Para excluir um time você precisa informar na URL do endpoint um {id} válido.

###### =====================================================================================

### COMPETIÇÃO
 Este módulo é resposánvel por controlar as competições da aplicação

##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: db5fb842672bb7fc1a105cf3ff645e861be2e05853a73cc022544c6c20649e57|
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR COMPETIÇÕES - @GET
 ```url
 http://localhost:3005/competition
 ```
Listar todas as competições: `http://localhost:3005/competition`

Listar competição única: `http://localhost:3005/competition/:id`

###### QueryString das filtragens disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| name           | Nome da competição                                                                                | 
| organization   | Organização responsável pela competição                                                           | 
| location       | Local em que a competição está ocorrendo                                                          |
| date_initial   | Data do início do evento                                                                          |
| date_end       | Data do fim do evento                                                                             |

Exemplo de pesquisa com querystring: `http://localhost:3005/competition?name=Libertadores&organization=Conmebol`

##### SALVAR COMPETIÇÃO - @POST
  ```url
 http://localhost:3005/competition
 ```
 
| Atributo        | Tipo    | Obrigatório |
| --------------- |---------|:-----------:| 
| name            | String  | Sim         |
| organization    | String  | Sim         |
| edition         | Number  | Sim         |
| system          | String  | Sim         |
| location        | String  | Sim         |
| date_initial    | Date    | Sim         |
| date_end        | Date    | Sim         |
| number_teams    | Number  | Sim         |
| premium_value   | Number  | Sim         |

##### ALTERAR COMPETIÇÃO - @PUT
```url
http://localhost:3005/competition/:id
```
Para alterar uma competição você precisa informar na URL do endpoint um {id} válido.

| Atributo        | Tipo   | Obrigatório | 
| --------------- |--------|:-----------:|
| name            | String | Sim         |
| organization    | String | Sim         |
| edition         | Number | Sim         |
| system          | String | Sim         |
| location        | String | Sim         |
| date_initial    | Date   | Sim         |
| date_end        | Date   | Sim         |
| number_teams    | Number | Sim         |
| premium_value   | Number | Sim         |

##### EXCLUIR COMPETIÇÃO - @DELETE
```url
http://localhost:3005/competition/:id
```
Para excluir uma competição você precisa informar na URL do endpoint um {id} válido.

###### =====================================================================================

### PARTIDA
 Este módulo é resposánvel por controlar as partidas da aplicação

##### Parâmetros de chamada
| Configuração  | Valor                                                                             | 
| ------------- |-----------------------------------------------------------------------------------| 
| Authorization | **Bearer Token**: db5fb842672bb7fc1a105cf3ff645e861be2e05853a73cc022544c6c20649e57|
| Headers       | **Content-type**: application/json                                                |

##### BUSCAR PARTIDAS - @GET
 ```url
 http://localhost:3005/game
 ```
Listar todas as partidas: `http://localhost:3005/game`

Listar partida única: `http://localhost:3005/game/:id`

###### QueryString das filtragens disponíveis
| Parâmetro      | Descrição                                                                                         | 
| -------------  |---------------------------------------------------------------------------------------------------| 
| date_game      | Data da partida                                                                                   | 
| status         | Situação da partida                                                                               | 

Exemplo de pesquisa com querystring: `http://localhost:3005/game?date_game=2020-11-03&status=ENCERRADA`

##### SALVAR PARTIDA - @POST
  ```url
 http://localhost:3005/game
```

| Atributo        | Tipo   | Obrigatório | 
| --------------- |--------|:-----------:| 
| id_team_one     | String | Sim         |
| id_team_two     | String | Sim         |
| id_competition  | String | Sim         |
| date_game       | Date   | Sim         |
| stadium         | String | Sim         |
| status          | String | Sim         |
| description     | String | Sim         |
| goals_team_one  | Number | Não         |
| goals_team_two  | Number | Não         |

##### ALTERAR PARTIDA - @PUT
```url
http://localhost:3005/game/:id
```
Para alterar uma partida você precisa informar na URL do endpoint um {id} válido.

| Nome            | Tipo   | Obrigatório | 
| --------------- |--------|:-----------:| 
| id_team_one     | String | Sim         |
| id_team_two     | String | Sim         |
| id_competition  | String | Sim         |
| date_game       | Date   | Sim         |
| stadium         | String | Sim         |
| status          | String | Sim         |
| description     | String | Sim         |
| goals_team_one  | Number | Não         |
| goals_team_two  | Number | Não         |

##### EXCLUIR PARTIDA - @DELETE
```url
http://localhost:3005/game/:id
```
Para excluir uma partida você precisa informar na URL do endpoint um {id} válido.