const express = require('express');
const cors = require('cors');
const app = express();

const routes = {
  'auth': require('./src/api/middleware/auth'),
  'team': require('./src/api/routes/team'),
  'competition': require('./src/api/routes/competition'),
  'game': require('./src/api/routes/game'),    
}

app.listen(process.env.PORT || 3005, function() {
  console.log('Aplicação rodando!');
});
app.use(cors());
app.use(routes.auth);
app.use(express.json());
app.use('/team', routes.team);
app.use('/competition', routes.competition);
app.use('/game', routes.game);

//Token registrado no banco de dados: db5fb842672bb7fc1a105cf3ff645e861be2e05853a73cc022544c6c20649e57